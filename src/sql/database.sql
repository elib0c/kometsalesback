CREATE DATABASE IF NOT EXISTS `komet`;

CREATE TABLE `komet`.`sellers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `commission_percentage` decimal(3,2) DEFAULT NULL,
  `avatar` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

CREATE TABLE `komet`.`sales` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `seller_id` bigint(20) NOT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `product_name` varchar(45) DEFAULT NULL,
  `amount` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `seller_forgein_idx` (`seller_id`),
  CONSTRAINT `seller_forgein` FOREIGN KEY (`seller_id`) REFERENCES `sellers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;

LOCK TABLES `sellers` WRITE;
/*!40000 ALTER TABLE `sellers` DISABLE KEYS */;
INSERT INTO `sellers` VALUES
(1,'Eli J. Chavez','eli.chavez@alphas.technology',3.00,'https://api.adorable.io/avatars/285/1.png'),
(2,'Luisana Pernia','luisana@gmail.com',2.10,'https://api.adorable.io/avatars/285/2.png'),
(3,'Jonathan Zarpa','jona.zarpa@gmail.com',1.95,'https://api.adorable.io/avatars/285/3.png'),
(4,'Carlos Roman','roman1981@hotmail.com',0.33,'https://api.adorable.io/avatars/285/4.png');
/*!40000 ALTER TABLE `sellers` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `sales` WRITE;
INSERT INTO `sales`
(seller_id, date, product_name, amount)
VALUES
(1,'2020-02-07 15:49:03','Laptop: Acer Predator LX1900',800000.00);
UNLOCK TABLES;