package com.alphas.elibo.komet.controllers;

import com.alphas.elibo.komet.models.Seller;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class SellerControllerTest {

    @Autowired
    private SellerController sellerController;

    @Test
    void getSellerByIdExistent() {
        Optional<Seller> seller = sellerController.findById("1");
        assertThat(seller.isPresent()).isTrue();
    }

    @Test
    void getSellerByIdNotExistent() {
        Optional<Seller> seller = sellerController.findById("-1");
        assertThat(seller.isPresent()).isFalse();
    }

    @Test
    void getAllSuccess() {
        List<Seller> sellers = sellerController.getAll();
        assertThat(sellers).isNotEmpty();
    }
}
