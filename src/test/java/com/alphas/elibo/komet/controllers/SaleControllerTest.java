package com.alphas.elibo.komet.controllers;

import com.alphas.elibo.komet.models.Sale;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

@SpringBootTest
public class SaleControllerTest {

    @Autowired
    private SaleController saleController;

    @Autowired
    ResourceLoader resourceLoader;

    @Test
    void getAllSuccess() {
        List<Sale> sales = saleController.getAll();
        Assertions.assertThat(sales).isNotEmpty();
    }

    @Test
    void getSalesBySellerIdExistent() {
        List<Sale> seller = this.saleController.findBySellerId("1");
        Assertions.assertThat(seller).isNotEmpty();
    }

    @Test
    void getSalesBySellerIdNotExistent() {
        List<Sale> seller = saleController.findBySellerId("-1");
        Assertions.assertThat(seller).isEmpty();
    }

    @Test
    void uploadCsvSuccess() throws IOException {
        Resource resource = resourceLoader.getResource("classpath:dataDemo.csv");

        File file = resource.getFile();
        FileInputStream input = new FileInputStream(file);

        MultipartFile multipartFile = new MockMultipartFile("file.csv",
                file.getName(), "text/plain", input);

        saleController.upload(multipartFile);
    }

    @Test
    void uploadCsvFailure() throws IOException {
        saleController.upload(null);
    }
}
