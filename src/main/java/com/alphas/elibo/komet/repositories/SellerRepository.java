package com.alphas.elibo.komet.repositories;

import com.alphas.elibo.komet.models.Seller;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface SellerRepository extends JpaRepository<Seller, Long> {

    @Query(nativeQuery = true, value = "SELECT * FROM sellers")
    List<Seller> getAll();

    @Query(nativeQuery = true, value = "SELECT * FROM sellers WHERE id = :seller_id")
    Optional<Seller> findById(@Param("seller_id") Long sellerId);
}
