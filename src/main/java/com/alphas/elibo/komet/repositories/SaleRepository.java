package com.alphas.elibo.komet.repositories;

import com.alphas.elibo.komet.models.Sale;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;

public interface SaleRepository extends JpaRepository<Sale, Long> {

    @Query(nativeQuery = true, value = "SELECT * FROM sales")
    List<Sale> getAll();

    @Query(nativeQuery = true, value =
            "SELECT * FROM sales WHERE seller_id = :seller_id")
    List<Sale> findBySellerId(@Param("seller_id") Long sellerId);

    @Modifying
    @Transactional
    @Query(nativeQuery = true, value =
            "INSERT INTO sales (seller_id, date, product_name, amount) VALUES (:seller_id, :d, :product_name, :amount)")
    Integer insert(@Param("seller_id") Long sellerId, @Param("d") Timestamp d, @Param("product_name") String productName, @Param("amount") Double amount);
}
