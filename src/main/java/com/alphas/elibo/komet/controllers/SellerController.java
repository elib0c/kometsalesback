package com.alphas.elibo.komet.controllers;

import com.alphas.elibo.komet.models.Seller;
import com.alphas.elibo.komet.repositories.SellerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "seller")
public class SellerController {

    @Autowired
    private SellerRepository sellerRepository;

    @GetMapping("{id}")
    public Optional<Seller> findById(@PathVariable String id) {
        return this.sellerRepository.findById(Long.parseLong(id));
    }

    @GetMapping
    public List<Seller> getAll() {
        return this.sellerRepository.getAll();
    }
}
