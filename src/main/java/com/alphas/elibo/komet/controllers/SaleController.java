package com.alphas.elibo.komet.controllers;

import com.alphas.elibo.komet.models.Sale;
import com.alphas.elibo.komet.repositories.SaleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping(path = "sale")
public class SaleController {

    @Autowired
    private SaleRepository saleRepository;

    @GetMapping
    public List<Sale> getAll() {
        return this.saleRepository.getAll();
    }

    @GetMapping("{id}")
    public List<Sale> findBySellerId(@PathVariable("id") String id) {
        return this.saleRepository.findBySellerId(Long.parseLong(id));
    }

    @Async
    @PostMapping(value = "upload", consumes = "multipart/form-data")
    public Exception upload(@RequestParam("file") MultipartFile file) {
        Exception response = new Exception("Error desconocido");
        if (file != null) {
            String line = "";
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(file.getInputStream()));
                while ((line = br.readLine()) != null) {
                    String[] sale = line.split(",");

                    try {
                        this.saleRepository.insert(
                                Long.parseLong(sale[0]),
                                Timestamp.valueOf(sale[1]),
                                sale[2],
                                Double.parseDouble(sale[3])
                        );
                    }  catch (IllegalArgumentException e) {
                        e.printStackTrace();

                        response = e;
                    }

                    response = null;
                }
            } catch (IOException e) {
                response = e;
            }
        }

        return response;
    }
}
