package com.alphas.elibo.komet.models;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;

@Data
@Entity
public class Sale {

    @Id
    private Long id;

    @Column(name = "seller_id")
    private Long sellerId;

    @Column
    private Timestamp date;

    @Column(name = "product_name")
    private String productName;

    @Column
    private Double amount;
}
