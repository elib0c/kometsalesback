package com.alphas.elibo.komet.models;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class Seller {

    @Id
    private Long id;

    @Column
    private String name;

    @Column
    private String email;

    @Column
    private String avatar;

    @Column(name = "commission_percentage")
    private Double commissionPercentage;
}
